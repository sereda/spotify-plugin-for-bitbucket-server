package com.atlassian.bitbucket.server.spotify.rest;


import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.fugue.Maybe;
import com.atlassian.bitbucket.server.spotify.TrackAssociationManager;
import com.sun.jersey.api.Responses;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * Gets and sets the Spotify track associated with a specific Pull Request.
 */
@Path("/associations/" + ResourcePatterns.PULL_REQUEST_URI)
@Produces("text/plain")
public class TrackAssociationsResource
{
    private final TrackAssociationManager trackAssociationManager;

    public TrackAssociationsResource(final TrackAssociationManager trackAssociationManager)
    {
        this.trackAssociationManager = trackAssociationManager;
    }

    @GET
    public Response get(@Context PullRequest pullRequest)
    {
        Maybe<String> association = trackAssociationManager.getAssociation(pullRequest);
        if (!association.isDefined())
        {
            return Responses.notFound().build();
        }

        return Response.ok(association.get()).build();
    }

    @PUT
    public Response put(@Context PullRequest pullRequest, final String body)
    {
        trackAssociationManager.associate(body, pullRequest);
        return Response.ok().build();
    }
}
