package it.com.atlassian.bitbucket.server.spotify;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.element.IdOption;

import org.openqa.selenium.By;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
public class TrackOption extends IdOption
{
    public TrackOption(@Nonnull PageElement container)
    {
        super(container);
    }

    public String getHref()
    {
        String attribute = container.find(By.className("spotify-avatar")).getAttribute("data-track-href");
        System.out.println("Found track with href " + attribute);
        return attribute;
    }

    @Nullable
    @Override
    public String getId()
    {
        return getHref();
    }
}