package it.com.atlassian.bitbucket.server.spotify;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.atlassian.bitbucket.test.RepositoryTestHelper;
import com.atlassian.bitbucket.test.TestContext;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.element.IdOption;
import com.atlassian.webdriver.bitbucket.element.IdSelect2;
import com.atlassian.webdriver.bitbucket.element.Select2;
import com.atlassian.webdriver.bitbucket.page.PullRequestCreatePage;
import com.atlassian.webdriver.bitbucket.page.PullRequestOverviewPage;
import com.atlassian.webdriver.utils.Check;

import it.com.atlassian.bitbucket.server.BaseFuncTest;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.bitbucket.test.GitTestHelper.getGitPath;
import static com.atlassian.bitbucket.test.ProcessTestHelper.execute;

public class TrackAcceptanceTest extends BaseFuncTest
{
    private static final String TEST_PR_AUTHOR = "prauthor";
    private static final String TEST_PR_REVIEWER = "prreviewer";
    private static final String TEST_ANOTHER_USER = "someguy";
    private static final String TEST_GROUP_NAME = "spotify-users";

    private static final String TEST_PROJECT_KEY = "SPOTIFY";
    private static final String TEST_REPO_NAME = "repo";

    private static final String TEST_SOURCE_BRANCH = "feature";
    private static final String TEST_DEST_BRANCH = "master";

    @Rule
    public final TestContext context = new TestContext();

    @Rule
    public final TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void init() throws Exception
    {
        context.user(TEST_PR_AUTHOR, "password")
               .user(TEST_PR_REVIEWER, "password")
               .user(TEST_ANOTHER_USER, "password")
               .group(TEST_GROUP_NAME, TEST_PR_AUTHOR, TEST_PR_REVIEWER, TEST_ANOTHER_USER)

               .project(TEST_PROJECT_KEY)

               .repository(TEST_PROJECT_KEY, TEST_REPO_NAME)
               .repositoryPermissionForGroup(TEST_PROJECT_KEY, TEST_REPO_NAME, TEST_GROUP_NAME, Permission.REPO_WRITE);

        createGitRepo();
    }

    private void createGitRepo() throws IOException, URISyntaxException
    {
        File repoDir = tempFolder.newFolder("repo");

        execute(repoDir, getGitPath(), "init");
        FileUtils.writeStringToFile(new File(repoDir, "README.md"), "This is a test");
        execute(repoDir, getGitPath(), "add", ".");
        execute(repoDir, getGitPath(), "commit", "-m", "Initial commit");
        execute(repoDir, getGitPath(), "checkout", "-b", TEST_SOURCE_BRANCH);
        FileUtils.writeStringToFile(new File(repoDir, "password.txt"), "letmein");
        execute(repoDir, getGitPath(), "add", ".");
        execute(repoDir, getGitPath(), "commit", "-m", "Adding password file to repo");


        RepositoryTestHelper.pushRep(repoDir, DefaultFuncTestData.getBaseURL(), DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), TEST_PROJECT_KEY, TEST_REPO_NAME);
    }

    @Test
    public void testCreatePullRequestWithTrack() throws Exception
    {
        PullRequestCreatePage prPage = loginAsAdmin(PullRequestCreatePage.class, TEST_PROJECT_KEY, TEST_REPO_NAME);

        prPage.getSourceRepositorySelector().open().selectItemByName(TEST_PROJECT_KEY + "/" + TEST_REPO_NAME);
        prPage.getSourceBranchSelector().open().selectItemByName(TEST_SOURCE_BRANCH);

        prPage = prPage.clickContinue();

        TrackSelect trackSelector = BITBUCKET.getPageBinder().bind(TrackSelect.class, prPage.getDetails().find(By.id("s2id_spotify-trackId")));
        trackSelector.type("Inhaler").selectOption("spotify:track:3tRqWoZuQtOfG54Vd6nNdq");
        PullRequestOverviewPage prOverview = prPage.clickSubmit();

        assertTrue(prOverview.isHere());

        WebDriver driver = BITBUCKET.getTester().getDriver();
        assertTrue(Check.elementExists(By.xpath("//a[@href=\"spotify:track:3tRqWoZuQtOfG54Vd6nNdq\"]"), driver));
    }


}
